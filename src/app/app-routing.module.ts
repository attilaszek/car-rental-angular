import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomeModule'},
  { path: 'users', loadChildren: './user/user.module#UserModule' },
  { path: 'cars', loadChildren: './car/car.module#CarModule' },
  { path: 'reservations', loadChildren: './reservation/reservation.module#ReservationModule'},
  { path: 'query', loadChildren: './query/query.module#QueryModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
