import { Component, OnInit } from '@angular/core';

import { Car } from '../../core/models/car.model'
import { User } from '../../core/models/user.model'
import { QueryService } from '../../core/services/query.service'
import { UserService } from '../../core/services/user.service'

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  private cars: Car[] = []

  private users: User[] = []
  //private user: User = new User()
  private user_id: number = null

  constructor(
    private queryService: QueryService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsersList()
      .subscribe(
        result => this.users = result
      )
  }

  getReservedCars() {
    if (!this.user_id) return
    this.queryService.getReservedCars(this.user_id)
      .subscribe(
        result => this.cars = result,
        error => console.log(error)
      )
  }

}
