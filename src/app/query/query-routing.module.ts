import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { CarsComponent } from './cars/cars.component'
import { UsersComponent } from './users/users.component'

import { AdminGuard } from '../core/guards/admin.guard'

const routes: Routes = [
  {
    path: 'cars',
    component: CarsComponent,
    canActivate: [AdminGuard] 
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AdminGuard] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueryRoutingModule { }
