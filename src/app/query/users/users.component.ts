import { Component, OnInit } from '@angular/core';

import { User } from '../../core/models/user.model'
import { QueryService } from '../../core/services/query.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  private users: User[] = []

  constructor(
    private queryService: QueryService
  ) { }

  ngOnInit() {
    this.getUsersWithReservations()
  }

  getUsersWithReservations() {
    this.queryService.getUsersWithReservations()
      .subscribe(
        result => this.users = result,
        error => console.log(error)
      )
  }

}
