import { Component, OnInit, Input } from '@angular/core';

import { Reservation } from '../../core/models/reservation.model'
import { ReservationService } from '../../core/services/reservation.service'

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  @Input()
  user_id: number = null

  @Input()
  car_id: number = null

  private reservations: Reservation[] = []

  constructor(
    private reservationService: ReservationService
  ) { }

  ngOnInit() {
    this.getReservations()
  }

  getReservations() {
    this.reservationService.getReservations(this.user_id, this.car_id)
      .subscribe(
        response => {
          this.reservations = response
          console.log(response)
        }
      )
  }

}
