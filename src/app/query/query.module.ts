import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { QueryRoutingModule } from './query-routing.module'
import { CarsComponent } from './cars/cars.component';
import { UsersComponent } from './users/users.component';
import { ReservationComponent } from './reservation/reservation.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    QueryRoutingModule
  ],
  declarations: [CarsComponent, UsersComponent, ReservationComponent]
})
export class QueryModule { }
