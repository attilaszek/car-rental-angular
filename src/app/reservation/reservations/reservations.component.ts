import { Component, OnInit } from '@angular/core';

import { Reservation } from '../../core/models/reservation.model'
import { ReservationService } from '../../core/services/reservation.service'

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  private reservations: Reservation[]

  constructor(
    private reservationService: ReservationService
  ) { }

  ngOnInit() {
    this.getReservations()
  }

  getReservations(): void {
    this.reservationService.getReservations(null, null)
      .subscribe(
        response => {
          this.reservations = response
          console.log(this.reservations)
        }
      )
  }

  delete(reservation: Reservation): void {
    this.reservationService.delete(reservation)
      .subscribe(
        respone => {
          var index = this.reservations.indexOf(reservation)
          this.reservations.splice(index, 1)
        },
        error => {
          alert("Cannot cancel this reservation")
        }
      )
  }

  addRate(reservation: Reservation): void {
    if (!reservation.rate) return
    this.reservationService.addRate(reservation)
      .subscribe()
  }

}
