import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ReservationsComponent } from './reservations/reservations.component'

import { AuthGuard } from '../core/guards/auth.guard'

const routes: Routes = [
  {
    path: '',
    component: ReservationsComponent,
    canActivate: [AuthGuard] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule { }
