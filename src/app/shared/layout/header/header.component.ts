import { Component, OnInit } from '@angular/core';

import { User } from '../../../core/models/user.model'
import { AuthService } from '../../../core/services/auth.service'

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: User = null

  email: string = ""
  password: string =""
  emailError: boolean = false
  passwordError: boolean = false

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.getCurrentUser()
      .subscribe(
        response => {
          this.currentUser = response
          console.log(response)
        }
      )
  }

  login(): void {  
    this.authService.login(this.email, this.password)
      .subscribe(
        response => {
          this.resetForm()
        },
        error => {
          this.emailError = error.email
          this.passwordError = error.password
          this.password = ""
        }
      )
    return null
  }  

  resetForm(): void {
    this.email = ""
    this.password = ""
    this.emailError = false
    this.passwordError = false
  }

  logout(): void {
    this.authService.logout()
  }
}