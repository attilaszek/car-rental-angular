import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AuthGuard } from '../core/guards/auth.guard'
import { AdminGuard } from '../core/guards/admin.guard'

import { CarsComponent } from './cars/cars.component'
import { CarDetailComponent } from './car-detail/car-detail.component'
import { NewCarComponent } from './new-car/new-car.component'
import { EditCarComponent } from './edit-car/edit-car.component'

const routes: Routes = [
  {
    path: '',
    component: CarsComponent,
    canActivate: [AuthGuard] 
  },
  {
    path: 'show/:id',
    component: CarDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'new',
    component: NewCarComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'edit/:id',
    component: EditCarComponent,
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarRoutingModule { }
