import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { Car } from '../../core/models/car.model'
import { CarService } from '../../core/services/car.service'
import { CarFormComponent } from '../car-form/car-form.component'

@Component({
  selector: 'app-new-car',
  templateUrl: './new-car.component.html',
  styleUrls: ['./new-car.component.css']
})
export class NewCarComponent implements OnInit {

  car: Car = new Car()
  errors: any

  constructor(
    private carService: CarService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  handleSubmit(car: Car): void {
    this.create(car)
  }

  create(car: Car) {
    this.carService.create(car)
      .subscribe(
        response => {
          this.router.navigateByUrl('cars')
        },
        error => {
          this.errors = error
        }
      )
  }

}
