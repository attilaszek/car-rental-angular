import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Car } from '../../core/models/car.model'
import { CarService } from '../../core/services/car.service'
import { UtilsService } from '../../core/services/utils.service'

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {

  private car: Car

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.getCar()
  }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id')
    this.carService.getCar(id)
      .subscribe(car => this.car = car)
  }

}
