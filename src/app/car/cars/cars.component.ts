import * as geojson from "geojson"

import { Component, OnInit } from '@angular/core';

import { Car } from '../../core/models/car.model'
import { CarService } from '../../core/services/car.service'
import { QueryService } from '../../core/services/query.service'
import { User } from '../../core/models/user.model'
import { AuthService } from '../../core/services/auth.service'
import { ReservationService } from '../../core/services/reservation.service'

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  private cars: Car[]
  private errors: any = new Object()
  private hoveredCar: Car

  private colors: string[]

  private search: {
    name: string,
    color: string,
    start_date: Date,
    end_date: Date
  } = {
    name: "",
    color: "",
    start_date: null,
    end_date: null
  }

  private currentUser: User

  constructor(
    private carService: CarService,
    private queryService: QueryService,
    private authService: AuthService,
    private reservationService: ReservationService
  ) { }

  ngOnInit() {
    this.getCarsList()
    this.getColors()
    this.getCurrentUser()
  }

  handleHover(car: Car) {
    this.hoveredCar = car
  }

  resetHover() {
    this.hoveredCar = null
  }

  searchCars() {
    this.queryService.filterCars(this.search)
      .subscribe(
        cars => {
          this.cars = cars
          this.errors = new Object()
        },
        response => this.errors = response.error
      )
  }

  delete(car: Car) {
    this.carService.delete(car)
      .subscribe(
        response => {
          var index = this.cars.indexOf(car)
          this.cars.splice(index, 1)
        },
        error => alert("An error occured while deleting car")
      )
  }

  makeReservation(car: Car): void {
    this.reservationService.makeReservation(this.search.start_date, this.search.end_date, car.id)
      .subscribe(
        result => this.searchCars(),
        response => {
          this.errors = response.error
        }
      )
  }

  getCarsList() {
    this.carService.getCarsList()
      .subscribe(
        response => this.cars = response
      )
  }

  getColors():void {
    this.carService.getColors()
      .subscribe(
        response => this.colors = Object.keys(response)
      )
  }

  getCurrentUser(): void {
    this.authService.getCurrentUser()
      .subscribe(
        response => {
          this.currentUser = response
          console.log(response)
        }
      )
  }
}
