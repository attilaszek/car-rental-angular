import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CarRoutingModule } from './car-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';

import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { NewCarComponent } from './new-car/new-car.component';
import { CarFormComponent } from './car-form/car-form.component';
import { EditCarComponent } from './edit-car/edit-car.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CarRoutingModule,
    NgbModule,

    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiYXR0aWxhc3playIsImEiOiJjampxdDRlOTY4YzNuM3BwMWQzYXNwem80In0.J3mwOlnRZWN4EC9W658cbg'
    })
  ],
  declarations: [CarsComponent, CarDetailComponent, NewCarComponent, CarFormComponent, EditCarComponent]
})
export class CarModule { }
