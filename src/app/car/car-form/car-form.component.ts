import * as geojson from "geojson"

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Car } from '../../core/models/car.model'
import { CarService } from '../../core/services/car.service'

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  @Input()
  car: Car

  @Output()
  submitted = new EventEmitter<Car>()

  colors: string[] = []

  constructor(
    private carService: CarService
  ) { }

  ngOnInit() {
    this.getColors()
  }

  handleFileChange(event: any): void {
    this.car.uploadedImage = event.target.files[0]

    const reader = new FileReader();
    reader.onload = e => this.car.image = reader.result;

    reader.readAsDataURL(this.car.uploadedImage);
  }

  handleClickOnMap(event: any): void {
    if (!event.lngLat) return
    this.car.latitude = event.lngLat.lat
    this.car.longitude = event.lngLat.lng
  }

  handleSubmit() {
    this.submitted.emit(this.car)
  }

  getColors():void {
    this.carService.getColors()
      .subscribe(
        response => this.colors = Object.keys(response)
      )
  }

}
