import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'

import { Car } from '../../core/models/car.model'
import { CarService } from '../../core/services/car.service'
import { CarFormComponent } from '../car-form/car-form.component'
import { UtilsService } from '../../core/services/utils.service'

@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {

  car: Car = new Car()
  errors: any

  constructor(
    private carService: CarService,
    private utils: UtilsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getCar()
  }

  handleSubmit(car: Car): void {
    this.update(car)
  }

  update(car: Car) {
    this.carService.update(car)
      .subscribe(
        response => {
          this.router.navigateByUrl('cars')
        },
        error => {
          this.errors = error
        }
      )
  }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id')
    this.carService.getCar(id)
      .subscribe(car => {
        this.car = car
        this.car.image = this.utils.serverUrl + this.car.image
      })
  }

}
