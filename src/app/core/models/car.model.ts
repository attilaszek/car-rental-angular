export class Car {
  id: number
  name: string
  color: string
  weight: number
  latitude: number
  longitude: number
  image: any
  uploadedImage: File
}