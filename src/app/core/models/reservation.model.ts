export class Reservation {
  id: number
  car_id: number
  car_name: string
  user_id: number
  user_name: string
  start_date: Date
  end_date: Date
  rate: number
}