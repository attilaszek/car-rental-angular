import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http'

import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public apiUrl: string
  public serverUrl: string

  constructor() {
    this.apiUrl = environment.apiUrl
    this.serverUrl = environment.serverUrl
  }

  getHttpOptions() {
    return {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('auth_token')
      })
    }
  }
}
