import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http'
import { RequestOptions } from '@angular/http'
import { Observable, BehaviorSubject, of, throwError } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'

import { User } from '../models/user.model'
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersUrl = `${this.utils.apiUrl}/users`

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  signup(user: User): Observable<any> {
    const url = `${this.usersUrl}/signup`
    return this.http.post<User>(url, user, this.utils.getHttpOptions())
    .pipe(
      tap(response => console.log(response)),
      catchError(response => {
        console.log(response)
        return throwError(response.error)
      })
    )
  }

  update(user: User): Observable<any> {
    const url = `${this.usersUrl}/update`
    return this.http.put<User>(url, user, this.utils.getHttpOptions())
    .pipe(
      tap(response => console.log(response)),
      catchError(response => {
        console.log(response)
        return throwError(response.error)
      })
    )
  }

  getUser(id: number): Observable<User> {
    const url = `${this.usersUrl}/show`
    const options: {
      headers: HttpHeaders,
      params: any
    } = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {id: id}
    }
    return this.http.get<User>(url, options)
  }

  getUsersList(): Observable<User[]> {
    const url = `${this.usersUrl}/index`
    return this.http.get<User[]>(url, this.utils.getHttpOptions())
  }

  changeAdminRole(id: number, admin: boolean): Observable<any> {
    const command = admin ? 'add_admin_role' : 'remove_admin_role'
    const url = `${this.usersUrl}/${command}`
    return this.http.put(url, {id: id}, this.utils.getHttpOptions())
      .pipe(
        tap(response => console.log(response)),
        catchError(response => {
          console.log(response)
          return throwError(response.error)
        })
      )
  }

  delete(id: number): Observable<any> {
    const url = `${this.usersUrl}`

    const options: any = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {id: id}
    }

    return this.http.delete(url, options)
  }
}
