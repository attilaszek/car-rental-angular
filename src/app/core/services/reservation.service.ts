import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

import { Reservation } from '../models/reservation.model'
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private reservationsUrl = `${this.utils.apiUrl}/reservations`

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  getReservations(user_id: number, car_id: number): Observable<Reservation[]> {
    let url = `${this.reservationsUrl}/index`

    let params: any = new Object()
    if (user_id) {
      params.user_id = user_id
    }
    if (car_id) {
      params.car_id = car_id
    }

    const options: {
      headers: HttpHeaders,
      params: any
    } = {
      headers: this.utils.getHttpOptions()['headers'],
      params: params
    }

    return this.http.get<Reservation[]>(url, options)
  }

  delete(reservation: Reservation): Observable<any> {
    const options: any = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {id: reservation.id}
    }

    return this.http.delete(this.reservationsUrl, options)
  }

  makeReservation(start_date: Date, end_date: Date, car_id: number): Observable<any> {
    let url = `${this.reservationsUrl}/new`

    let reservation = new Reservation()
    reservation.start_date = start_date
    reservation.end_date = end_date
    reservation.car_id = car_id

    return this.http.post(url, reservation, this.utils.getHttpOptions())
  }

  addRate(reservation: Reservation) {
    let url = `${this.reservationsUrl}/rate`

    return this.http.put(url, reservation, this.utils.getHttpOptions())
  }
}
