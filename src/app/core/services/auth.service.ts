import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of, throwError } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http'

import { User } from '../models/user.model'
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private currentUser: BehaviorSubject<User> = new BehaviorSubject<User>(null)

  private authUrl = `${this.utils.apiUrl}/users`

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  getCurrentUser(token = null): BehaviorSubject<User> {
    if (token) localStorage.setItem('auth_token', token)
    if ((localStorage.getItem('auth_token') && !this.currentUser.value) || token) {
      this.setCurrentUser()
    }
    return this.currentUser
  }

  isAuthenticated(): boolean {
    if (this.currentUser.value) return true
    return false
  }

  login(email: string, password: string): Observable<any> {
    const url = `${this.authUrl}/login`
    var params = {email: email, password: password}
    return this.http.post<any>(url, params, this.utils.getHttpOptions())
      .pipe(
        tap(response => {
          console.log(response)
          localStorage.setItem('auth_token', response.token)
          this.setCurrentUser()
        }),
        catchError(response => {
          console.log(response.error)
          return throwError(response.error)
        })
      )
  }

  logout(): void {
    localStorage.setItem('auth_token', "")
    this.currentUser.next(null)
  }

  setCurrentUser(): void {
    const url = `${this.authUrl}/get_current_user`
    this.http.get<User>(url, this.utils.getHttpOptions())
      .subscribe(
        response => {
          this.currentUser.next(response)
          console.log(this.currentUser)
        }
      )
  }
}