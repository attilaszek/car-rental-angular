import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

import { Car } from '../models/car.model'
import { User } from '../models/user.model'
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  private queryUrl = `${this.utils.apiUrl}/query`

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  filterCars(searchTerms: any):Observable<Car[]> {
    const url = `${this.queryUrl}/filter_cars`

    var newSearchTerms: any = new Object()
    for (var key in searchTerms) {
        if (searchTerms[key]) {
        newSearchTerms[key] = searchTerms[key]
      }
    }

    if (!newSearchTerms.start_date) delete newSearchTerms.end_date
    if (!newSearchTerms.end_date) delete newSearchTerms.start_date

    const options: {
      headers: HttpHeaders,
      params: any
    } = {
      headers: this.utils.getHttpOptions()['headers'],
      params: newSearchTerms
    }
    return this.http.get<Car[]>(url, options)
  }

  getUsersWithReservations(): Observable<User[]> {
    const url = `${this.queryUrl}/user_list`

    return this.http.get<User[]>(url, this.utils.getHttpOptions())
  }

  getReservedCars(user_id: number): Observable<Car[]> {
    const url = `${this.queryUrl}/reserved_cars`

    const options: {
      headers: HttpHeaders,
      params: any
    } = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {user_id: user_id}
    }

    return this.http.get<Car[]>(url, options)
  }
}
