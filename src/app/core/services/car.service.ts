import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

import { Car } from '../models/car.model'
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private carsUrl = `${this.utils.apiUrl}/cars`

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  getCarsList(): Observable<Car[]> {
    const url = `${this.carsUrl}/index`
    return this.http.get<Car[]>(url, this.utils.getHttpOptions())
  }

  getCar(id: number): Observable<Car> {
    const url = `${this.carsUrl}/show`
    const options: {
      headers: HttpHeaders,
      params: any
    } = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {id: id}
    }
    return this.http.get<Car>(url, options)
  }

  create(car: Car): Observable<any> {
    const url = `${this.carsUrl}/new`

    let formData: FormData = new FormData()

    formData.append("name", car.name)
    formData.append("color", car.color)
    formData.append("weight", car.weight.toString())
    formData.append("latitude", car.latitude.toString())
    formData.append("longitude", car.longitude.toString())
    formData.append("image", car.uploadedImage)


    let options = this.utils.getHttpOptions()
    options.headers = options.headers.delete('Content-Type')

    return this.http.post(url, formData, options)
    .pipe(
      tap(response => console.log(response)),
      catchError(response => {
        console.log(response)
        return throwError(response.error)
      })
    )
  }

  update(car: Car): Observable<any> {
    const url = `${this.carsUrl}/edit`

    let formData: FormData = new FormData()

    formData.append("id", car.id.toString())
    formData.append("name", car.name)
    formData.append("color", car.color)
    formData.append("weight", car.weight.toString())
    formData.append("latitude", car.latitude.toString())
    formData.append("longitude", car.longitude.toString())
    if (car.uploadedImage) formData.append("image", car.uploadedImage)


    let options = this.utils.getHttpOptions()
    options.headers = options.headers.delete('Content-Type')

    return this.http.put(url, formData, options)
    .pipe(
      tap(response => console.log(response)),
      catchError(response => {
        console.log(response)
        return throwError(response.error)
      })
    )
  }

  delete(car: Car): Observable<any> {
    const options: any = {
      headers: this.utils.getHttpOptions()['headers'],
      params: {id: car.id}
    }

    return this.http.delete(this.carsUrl, options)
  }

  getColors(): Observable<string[]> {
    const url = `${this.carsUrl}/colors`
    return this.http.get<string[]>(url, this.utils.getHttpOptions())
  }
}
