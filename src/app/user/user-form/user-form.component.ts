import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

import { User } from '../../core/models/user.model'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Input()
  user: User

  @Input()
  errors: any

  @Input()
  isUpdate: boolean = false

  @Output()
  submitted = new EventEmitter<User>()

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    this.submitted.emit(this.user)
  }

}
