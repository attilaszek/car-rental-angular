import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';

import { SignupComponent } from './signup/signup.component';
import { IndexComponent } from './index/index.component';
import { UserFormComponent } from './user-form/user-form.component';
import { NewUserComponent } from './new-user/new-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule
  ],
  declarations: [SignupComponent, IndexComponent, UserFormComponent, NewUserComponent, EditUserComponent]
})
export class UserModule { }