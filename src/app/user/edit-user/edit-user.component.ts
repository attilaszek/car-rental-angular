import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router'

import { User } from '../../core/models/user.model'
import { UserService } from '../../core/services/user.service'
import { UserFormComponent } from '../user-form/user-form.component'

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  user: User = new User()
  errors = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    password_confirmation: ""
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUser()
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id')
    this.userService.getUser(id)
      .subscribe(user => this.user = user)
  }

  update(user: User) {
    this.userService.update(user)
      .subscribe(
        response => {
          this.router.navigateByUrl('users')
        },
        error => {
          this.errors = error
        }
      )
  }

  handleSubmit(user: User): void {
    this.update(user)
  }

}
