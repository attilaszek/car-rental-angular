import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { User } from '../../core/models/user.model'
import { UserService } from '../../core/services/user.service'

import { AuthService } from '../../core/services/auth.service'

import { UserFormComponent } from '../user-form/user-form.component'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user: User = new User()
  errors = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    password_confirmation: ""
  }

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  signup(user: User) {
    this.userService.signup(user)
      .subscribe(
        response => {
          this.authService.getCurrentUser(response.token)
          this.router.navigateByUrl('home')
        },
        error => {
          this.errors = error
        }
      )
  }

  handleSubmit(user: User): void {
    this.signup(user)
  }
}
