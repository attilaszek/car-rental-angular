import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SignupComponent } from './signup/signup.component'
import { IndexComponent } from './index/index.component'
import { NewUserComponent } from './new-user/new-user.component'
import { EditUserComponent } from './edit-user/edit-user.component'

import { GuestGuard } from '../core/guards/guest.guard'
import { AdminGuard } from '../core/guards/admin.guard'

const routes: Routes = [
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [GuestGuard] 
  },
  {
    path: '',
    component: IndexComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'new',
    component: NewUserComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'edit/:id',
    component: EditUserComponent,
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
