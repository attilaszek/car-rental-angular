import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { User } from '../../core/models/user.model'
import { UserService } from '../../core/services/user.service'
import { UserFormComponent } from '../user-form/user-form.component'

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  user: User = new User()
  errors = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    password_confirmation: ""
  }

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  create(user: User) {
    this.userService.signup(user)
      .subscribe(
        response => {
          this.router.navigateByUrl('users')
        },
        error => {
          this.errors = error
        }
      )
  }

  handleSubmit(user: User): void {
    this.create(user)
  }

}