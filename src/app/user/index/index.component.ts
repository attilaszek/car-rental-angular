import { Component, OnInit } from '@angular/core';

import { User } from '../../core/models/user.model'
import { UserService } from '../../core/services/user.service'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private users: User[]

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsersList()
      .subscribe(
        response => {
          this.users = response
        }
      )
  }

  handleAdminChange(event: any, user: User): void {
    this.userService.changeAdminRole(user.id, event.target.checked)
      .subscribe(
        response => { },
        error => {
          var index = this.users.indexOf(user)
          this.users[index].admin = true
          this.users = this.users.slice()
          alert(error.error[0].user)
        }
      )
  }

  deleteUser(user: User): void {
    this.userService.delete(user.id)
      .subscribe(
        response => {
          var index = this.users.indexOf(user)
          this.users.splice(index, 1)
        },
        error => alert("An error occured while deleting user")
      )
  }

}
